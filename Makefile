CXX=clang++
CXXFLAGS=-Wall -Werror -Wextra -std=c++1y -pedantic -lsfml-graphics -lsfml-window -lsfml-system

BIN=tower_defense
SRC=src/game.cc src/tower_defense.cc\
		src/Map/map.cc src/Map/tile.cc src/Mob/mob.cc\
		src/Tower/tower.cc
OBJ=src/tower_defense.o

all:
	${CXX} ${CXXFLAGS} -o ${BIN} ${SRC}

clean:
	rm -f ${TARGET} ${BIN}
