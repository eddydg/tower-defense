#ifndef MOB_HH_
# define MOB_HH_
# include "../Map/map.hh"
class Mob
{
public:
    Mob(int health, float speed, int x, int y, int gold, int score);
    bool is_alive() const;
    int get_health() const;
    float get_speed() const;
    float get_x() const;
    float get_y() const;
    int get_gold() const;
    int get_score() const;
    void attacked(int damage);
    void print_mob(matrix map, sf::Sprite mob,
        sf::RenderWindow& window);
    void set_pos(sf::Sprite& m, matrix map);
protected:
    bool alive_;
    int health_;
    float speed_;
    int x_;
    int y_;
    int gold_;
    int score_;
    bool left_;
};

#endif /* !MOB_HH_ */
