#include "mob.hh"

Mob::Mob(int health, float speed, int x, int y, int gold, int score)
    : alive_(true)
    , health_(health)
    , speed_(speed)
    , x_(x)
    , y_(y)
    , gold_(gold)
    , score_(score)
    , left_(false)
{}

bool Mob::is_alive() const
{
  return alive_;
}

int Mob::get_score() const
{
  return score_;
}
int Mob::get_gold() const
{
  return gold_;
}

int Mob::get_health() const
{
  return health_;
}

float Mob::get_speed() const
{
  return speed_;
}

float Mob::get_x() const
{
  return x_;
}

float Mob::get_y() const
{
  return y_;
}

void Mob::attacked(int damage)
{
  health_ -= damage;
  if (health_ < 0)
    alive_ = false;
}

void Mob::set_pos(sf::Sprite& m, matrix map)
{
  int x = x_ / BLOCK_SIZE;
  int y = y_ / BLOCK_SIZE;
  if (left_ == true && map[y][x].get_dir() != Direction::LEFT)
    x = (x_ + BLOCK_SIZE) / BLOCK_SIZE;
  switch (map[y][x].get_dir())
  {
    case Direction::UP:
      y_ -= 6;
      break;
    case Direction::DOWN:
      y_ += 6;
      break;
    case Direction::LEFT:
      x_ -= 6;
      break;
    case Direction::RIGHT:
      x_ += 6;
      break;
    case Direction::NONE:
      break;
  }
  m.setPosition(x_, y_);
}
void Mob::print_mob(matrix map, sf::Sprite mob,
                    sf::RenderWindow& window)
{
  if (map[y_ / BLOCK_SIZE][x_ / BLOCK_SIZE].get_dir() == Direction::LEFT)
    left_ = true;
  if (x_ != (int)(BLOCK_SIZE * (map.size() - 1))
      || map[y_ / BLOCK_SIZE][x_ / BLOCK_SIZE].get_tile_type()
      != Tile_type::END)
  {
    set_pos(mob, map);
    window.draw(mob);
  }
  else
  {
    x_ = 0;
    y_ = BLOCK_SIZE;
    left_ = false;
  }
}
