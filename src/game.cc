#include "game.hh"

Game::Game()
{
  in_pause_ = false;
  curr_level_ = 0;
  score_ = 0;
  num_tower_ = 0;
  num_mobs_ = 0;
  clicked_ = false;
  if (!font_.loadFromFile("src/ressources/boombox2.ttf"))
    std::cout << "Couldn't load the font..." << std::endl;
}

void Game::set_score(int score)
{
  score_ = score;
}

void Game::add_score(int score)
{
  score_ += score;
}

int Game::get_score()
{
  return score_;
}

void Game::add_tower(Tower t)
{
  towers_.push_back(t);
  num_tower_++;
}

void Game::add_mob(Mob b)
{
  mobs_.push_back(b);
  num_mobs_++;
}

std::list<Tower> Game::get_towers()
{
  return towers_;
}

std::list<Mob> Game::get_mobs()
{
  return mobs_;
}

void Game::print_hud(sf::Vector2i win_size,
    sf::Event event,
    sf::RenderWindow& window)
{
  print_score(window);
  print_tower(win_size, event, window);
}

void Game::print_background(sf::RenderWindow& window)
{
  sf::Texture background;
  sf::Sprite tile;
  if (!background.loadFromFile("background.png"))
  {
    std::cout << "No background found" << std::endl;
    exit(1);
  }
  tile.setTexture(background);
  window.draw(tile);
}

void Game::print_score(sf::RenderWindow& window)
{
  sf::Text score;
  score.setFont(font_);
  score.setString("SCORE " + std::to_string(score_));
  score.setCharacterSize(24);
  score.setColor(sf::Color(255, 255, 255, 120));
  score.move(sf::Vector2f(10, 10));

  window.draw(score);
}

void Game::print_tower(sf::Vector2i win_size,
    sf::Event event,
    sf::RenderWindow& window)
{
  sf::Text text;
  text.setFont(font_);
  text.setString("TOWER");
  text.setCharacterSize(14);
  text.setColor(sf::Color::Yellow);
  sf::Vector2f tower_origin(50, win_size.x - 150);
  text.move(tower_origin);
  sf::Vector2f tower_size(70, 25);
  sf::Rect<float> tower_rect(tower_origin, tower_size);
  manage_events("Tower", tower_rect, event, window);

  window.draw(text);
}

void Game::manage_events(std::string name,
    sf::Rect<float> rect,
    sf::Event event,
    sf::RenderWindow& window)
{
  if (event.type == sf::Event::MouseButtonReleased)
    if (event.mouseButton.button == sf::Mouse::Left)
      clicked_ = false;

  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
  {
    clicked_ = false;
    selected_ = "";
    std::cout << "Unclicked " << name << std::endl;
  }

  if (clicked_on(rect, event, window) && !clicked_)
  {
    clicked_ = true;
    if (!selected_.empty())
    {
      selected_ = "";
      std::cout << "Unclicked " << name << std::endl;
    }
    else
    {
      selected_ = name;
      std::cout << "Clicked " << name << std::endl;
    }
  }
}

bool Game::clicked_on(sf::Rect<float> rect,
    sf::Event event,
    sf::RenderWindow& window)
{
  sf::Vector2i mouse_posi = sf::Mouse::getPosition(window);
  sf::Vector2f mouse_posf(mouse_posi.x, mouse_posi.y);


  if (event.type == sf::Event::MouseButtonPressed)
    if (event.mouseButton.button == sf::Mouse::Left)
      return rect.contains(mouse_posf);
  return false;
}

Tile Game::clicked_tile(matrix map, sf::RenderWindow& window)
{
  sf::Vector2i mouse_posi = sf::Mouse::getPosition(window);

  Tile tile_p(Direction::NONE,
              Tile_type::NONE,
              sf::Vector2i(0, 0),
              sf::Vector2i(0, 0));

  if (mouse_posi.x >= 0 && mouse_posi.y >= 0
      && mouse_posi.x < 10 * BLOCK_SIZE
      && mouse_posi.y < 10 * BLOCK_SIZE
     )
  {
    int x = mouse_posi.x / BLOCK_SIZE;
    int y = mouse_posi.y / BLOCK_SIZE;
    return map[y][x];
  }
  return tile_p;
}

void Game::place_tower(Tile tile, matrix& map)
{
  if (tile.get_tile_type() == Tile_type::WALL)
  {
    if (selected_ == "Tower")
    {
      sf::Vector2i tile_pos = tile.get_tile_pos();
      map[tile_pos.y][tile_pos.x].set_type(Tile_type::TOWER);
      map[tile_pos.y][tile_pos.x].set_tile_tex(1, 2);
    }
    else
      std::cout << "Please select a tower." << std::endl;
  }
  else
    std::cout << "You can't place a tower here!" << std::endl;
}
