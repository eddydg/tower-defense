#include "wave.hh"

Wave::Wave()
{
  num_mobs_ = 0;
}

void Wave::add_mob(Mob m)
{
  mobs_.push_back(m);
  num_mobs_++;
}

void Wave::remove_mob(Mob m)
{
  for (std::list<Mob>::iterator mob = mobs_.begin(); mob != mobs_.end();)
  {
    if (mob->get_pos() == m.get_pos())
    {
      mobs_.erase(mob);
      break;
    }
    else
      ++mob;
  }
  //mobs_.remove(m);
  num_mobs_--;
}

void Wave::set_level(int level)
{
  level_ = level;
}
