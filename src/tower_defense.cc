#include "game.hh"
#include "tower_defense.hh"
#include "screens.hpp"

int main()
{
  int width = 0;
  int height = 0;
  std::ifstream open_file("map.txt");

  if (open_file.is_open())
  {
    std::string tileset;
    open_file >> tileset;
    while (!open_file.eof())
    {
      std::string line;
      open_file >> line;
      if (height == 0)
        width = line.length();
      if (open_file.peek() == '\n')
        height++;
    }
  }
  int x = width * BLOCK_SIZE + 200;
  int y = height * BLOCK_SIZE;

  std::vector<cScreen*> screens;
  int screen = 0;

  sf::RenderWindow window(sf::VideoMode(x, y), "Geometry Defender");

  screen_0 s0;
  screens.push_back(&s0);
  screen_1 s1;
  screens.push_back(&s1);

  while (screen >= 0)
    screen = screens[screen]->Run(window);

  return EXIT_SUCCESS;
}
