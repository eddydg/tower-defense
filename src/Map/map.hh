#ifndef MAP_HH
# define MAP_HH

# include "../tower_defense.hh"
# include "tile.hh"

using matrix = std::vector<std::vector<Tile>>;

class Map
{
public:
  matrix get_map() const;
  void set_map(matrix map);
  void set_size(sf::Vector2i size);
  sf::Vector2i get_size();
  static std::vector<Tile> parse_line(std::string line,
                                      std::vector<Tile>& tmp_map,
                                      unsigned long width,
                                      int height, Direction& d);
  void print_map(sf::RenderWindow& window);
  static void set_map(matrix map, sf::Sprite tile, sf::RenderWindow& window);
protected:
  matrix map_;
  sf::Vector2i size_;
};

void check_map(matrix& map);

#endif /* !MAP_HH */
