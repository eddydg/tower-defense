#ifndef TILE_HH
# define TILE_HH

# include <SFML/Graphics.hpp>
# include <string>
# include <vector>

enum class Tile_type
{
  BEGIN,
  END,
  PATH,
  WALL,
  TURN,
  TOWER,
  NONE
};

enum class Direction
{
  UP,
  DOWN,
  LEFT,
  RIGHT,
  NONE
};

class Tile
{
public:
  Tile(Direction dir, Tile_type type, sf::Vector2i texture,
       sf::Vector2i position)
    : dir_(dir)
    , type_(type)
    , texture_(texture)
    , position_(position)
  {}

  sf::Vector2i get_tile_pos() const;
  Direction get_dir() const;
  void set_dir(Direction d);
  Tile_type get_tile_type() const;
  void set_type(Tile_type type);
  sf::Vector2i get_tile_tex() const;
  sf::Vector2i set_tile_tex(int x, int y);

protected:
  Direction dir_;
  Tile_type type_;
  sf::Vector2i texture_;
  sf::Vector2i position_;
};

#endif /* !TILE_HH */
