#include <cctype>
#include "tile.hh"


void Tile::set_dir(Direction d)
{
  dir_ = d;
}

Direction Tile::get_dir() const
{
  return dir_;
}

sf::Vector2i Tile::get_tile_pos() const
{
  return position_;
}

Tile_type Tile::get_tile_type() const
{
  return type_;
}

void Tile::set_type(Tile_type type)
{
  type_ = type;
}

sf::Vector2i Tile::get_tile_tex() const
{
  return texture_;
}

sf::Vector2i Tile::set_tile_tex(int x, int y)
{
  texture_ = sf::Vector2i(x, y);
  return texture_;
}
