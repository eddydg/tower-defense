#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include "map.hh"

matrix Map::get_map() const
{
  return map_;
}

void Map::set_map(matrix m)
{
  map_ = m;
}

void Map::set_size(sf::Vector2i size)
{
  size_ = size;
}

sf::Vector2i Map::get_size()
{
  return size_;
}

std::vector<Tile> Map::parse_line(std::string line, std::vector<Tile>& tmp_map,
                                  unsigned long width, int height, Direction& d)
{
  char c = line[width];
  switch (c)
  {
    case 'x':
      tmp_map.push_back(Tile(Direction::NONE, Tile_type::NONE,
            sf::Vector2i(2, 0), sf::Vector2i(width, height)));
      break;
    case 'w':
      tmp_map.push_back(Tile(Direction::NONE, Tile_type::WALL,
            sf::Vector2i(0, 0), sf::Vector2i(width, height)));
      break;
    case 'p':
      tmp_map.push_back(Tile(d, Tile_type::PATH,
            sf::Vector2i(1, 0), sf::Vector2i(width, height)));
      break;
    case 'B':
      tmp_map.push_back(Tile(d, Tile_type::BEGIN,
            sf::Vector2i(1, 0), sf::Vector2i(width, height)));
      break;
    case 'E':
      tmp_map.push_back(Tile(d, Tile_type::END,
            sf::Vector2i(3, 0), sf::Vector2i(width, height)));
      break;
    case 'u':
      tmp_map.push_back(Tile(Direction::UP, Tile_type::TURN,
            sf::Vector2i(1, 0), sf::Vector2i(width, height)));
      d = Direction::UP;
      break;
    case 'd':
      tmp_map.push_back(Tile(Direction::DOWN, Tile_type::TURN,
            sf::Vector2i(1, 0), sf::Vector2i(width, height)));
      d = Direction::DOWN;
      break;
    case 'l':
      tmp_map.push_back(Tile(Direction::LEFT, Tile_type::TURN,
            sf::Vector2i(1, 0), sf::Vector2i(width, height)));
      d = Direction::LEFT;
      break;
    case 'r':
      tmp_map.push_back(Tile(Direction::RIGHT, Tile_type::TURN,
            sf::Vector2i(1, 0), sf::Vector2i(width, height)));
      d = Direction::RIGHT;
      break;
  }
  return tmp_map;
}

void Map::set_map(matrix map, sf::Sprite tile, sf::RenderWindow& window)
{
  for (unsigned int i = 0; i < map.size(); ++i)
  {
    for(unsigned int j = 0; j < map[i].size(); ++j)
    {
      if (map[i][j].get_tile_pos().x != -1
          && map[i][j].get_tile_pos().y != -1)
      {
        tile.setPosition(j * BLOCK_SIZE, i * BLOCK_SIZE);
        tile.setTextureRect(sf::IntRect(map[i][j].get_tile_tex().x * BLOCK_SIZE,
              map[i][j].get_tile_tex().y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE));
        window.draw(tile);
      }
    }
  }
}

void check_map(matrix& map)
{
  bool tmp;
  int i = 0;
  for (unsigned int i = 0; i < map.size(); ++i)
  {
    tmp = false;
    for(int j = map[i].size() - 1; j >= 0; --j)
    {
      if (map[i][j].get_tile_type() == Tile_type::TURN &&
          map[i][j].get_dir() == Direction::LEFT)
        tmp = true;
      if (tmp == true && map[i][j].get_tile_type() == Tile_type::PATH)
        map[i][j].set_dir(Direction::LEFT);
    }
  }
  for (int j = map[i].size() - 1; j >= 0; --j)
  {
    tmp = false;
    for(unsigned int i = 0; i < map.size(); ++i)
    {
      if (map[i][j].get_tile_type() == Tile_type::TURN &&
          map[i][j].get_dir() == Direction::DOWN)
        tmp = true;
      if (map[i][j].get_tile_type() == Tile_type::TURN &&
          map[i][j].get_dir() != Direction::DOWN)
        tmp = false;
      if (tmp == true && map[i][j].get_tile_type() == Tile_type::PATH)
        map[i][j].set_dir(Direction::DOWN);
    }
  }
}
