#ifndef SCREENS_HPP
# define SCREENS_HPP

//Basic Screen Class
# include "cScreen.hpp"

//Including each screen of application
# include "screen_0.hpp"
# include "screen_1.hpp"

#endif /* !SCREENS_HPP */
