#ifndef GAME_HH_
# define GAME_HH_

# include <cstring>
# include "tower_defense.hh"
# include "Map/map.hh"
# include "Tower/tower.hh"
# include "Mob/mob.hh"

class Game
{
public:
  Game();

  void print_hud(sf::Vector2i win_size, sf::Event event,
                 sf::RenderWindow& window);
  void print_background(sf::RenderWindow& window);

  void set_score(int score);
  void add_score(int score);
  int  get_score();
  std::list<Tower> get_towers();
  std::list<Mob> get_mobs();
  Map get_map();

  void add_tower(Tower t);
  void add_mob(Mob b);
  void place_tower(Tile tile, matrix& map);
  //void remove_mob(Mob b);

  Tile clicked_tile(matrix map, sf::RenderWindow& window);

private:
  void print_score(sf::RenderWindow& window);
  void print_tower(sf::Vector2i win_size,
                   sf::Event event, sf::RenderWindow& window);
  bool clicked_on(sf::Rect<float> rect,
                  sf::Event event,
                  sf::RenderWindow& window);
  void manage_events(std::string name,
                     sf::Rect<float> rect,
                     sf::Event event,
                     sf::RenderWindow& window);
private:
  bool in_pause_;
  int curr_level_;
  int score_;

  int num_tower_;
  int num_mobs_;

  bool clicked_;
  std::string selected_;

  //Clicked item
  //Map map_;
  //std::list<Wave> waves_;
  std::list<Tower> towers_;
  std::list<Mob> mobs_;

  sf::Font font_;
};

#endif /* !GAME_HH_ */
