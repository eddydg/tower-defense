#include <iostream>
#include "cScreen.hpp"

#include <SFML/Graphics.hpp>

class screen_0 : public cScreen
{
  private:
    int alpha_max;
    int alpha_div;
    bool playing;
  public:
    screen_0(void);
    virtual int Run(sf::RenderWindow &window);
};

screen_0::screen_0(void)
{
  alpha_max = 1 * 255;
  alpha_div = 1;
  playing = false;
}
void check_menu(int& menu, sf::Text& menu1,
    sf::Text& menu2, sf::Text& menu3)
{
  if (menu == 0)
  {
    menu1.setColor(sf::Color(0, 226, 208, 255));
    menu2.setColor(sf::Color(255, 255, 255, 255));
    menu3.setColor(sf::Color(0, 226, 208, 255));
  }
  else
  {
    menu1.setColor(sf::Color(255, 255, 255, 255));
    menu2.setColor(sf::Color(0, 226, 208, 255));
    menu3.setColor(sf::Color(255, 255, 255, 255));
  }
}
void init_menu(sf::Text& menu1, sf::Text& menu2,
    sf::Text& menu3, sf::Font& font)
{
  menu1.setFont(font);
  menu1.setCharacterSize(40);
  menu1.setString("Play");
  menu1.setPosition({ 800, 300 });

  menu2.setFont(font);
  menu2.setCharacterSize(40);
  menu2.setString("Exit");
  menu2.setPosition({ 800, 380 });

  menu3.setFont(font);
  menu3.setCharacterSize(40);
  menu3.setString("Continue");
  menu3.setPosition({ 800, 300 });
}
int screen_0::Run(sf::RenderWindow &window)
{
  sf::Event Event;
  bool running = true;
  sf::Texture texture;
  sf::Sprite sprite;
  int alpha = 0;
  sf::Font font;
  sf::Text menu1;
  sf::Text menu2;
  sf::Text menu3;
  int menu = 0;
  if (!texture.loadFromFile("src/ressources/logo.png"))
  {
    std::cerr << "Error loading background" << std::endl;
    return -1;
  }
  sprite.setTexture(texture);
  sprite.setColor(sf::Color(255, 255, 255, alpha));
  if (!font.loadFromFile("src/ressources/UASQUARE.TTF"))
  {
    std::cerr << "Error loading " << std::endl;
    return -1;
  }
  init_menu(menu1, menu2, menu3, font);
  if (playing)
    alpha = alpha_max;
  while (running)
  {
    while (window.pollEvent(Event))
    {
      if (Event.type == sf::Event::Closed)
        return -1;
      if (Event.type == sf::Event::KeyPressed)
      {
        switch (Event.key.code)
        {
          case sf::Keyboard::Up:
            menu = 0;
            break;
          case sf::Keyboard::Down:
            menu = 1;
            break;
          case sf::Keyboard::Return:
            if (menu == 0)
            {
              playing = true;
              return 1;
            }
            else
              return -1;
            break;
          default:
            break;
        }
      }
    }
    if (alpha < alpha_max)
      alpha++;
    sprite.setColor(sf::Color(255, 255, 255, alpha / alpha_div));
    check_menu(menu, menu1, menu2, menu3);
    window.clear();
    window.draw(sprite);
    if (alpha == alpha_max)
    {
      if (playing)
        window.draw(menu3);
      else
        window.draw(menu1);
      window.draw(menu2);
    }
    window.display();
  }
  return -1;
}
