#ifndef WAVE_HH_
# define WAVE_HH_

# include "mob.hh"
# include <list>

class Wave
{
public:
  Wave();

  void add_mob(Mob m);
  void remove_mob(Mob m);
  void set_level(int level);

private:
  int num_mobs_;
  int level_;
  std::list<Mob> mobs_;
};

#endif /* !WAVE_HH_ */
