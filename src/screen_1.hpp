#include <iostream>
#include "cScreen.hpp"
#include "game.hh"
#include "tower_defense.hh"

#include <SFML/Graphics.hpp>

class screen_1 : public cScreen
{
  public:
    virtual int Run(sf::RenderWindow &window);
};

void print_all_mob(Mob& monster, Mob& monster1, Mob& monster2,
    sf::RenderWindow& window, sf::Clock clock,
    sf::Sprite& mob, sf::Sprite& mob1, sf::Sprite& mob2, matrix map)
{
  mob.setPosition(0, 0);
  mob.setTextureRect(sf::IntRect(BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE,
        BLOCK_SIZE));
  mob1.setTextureRect(sf::IntRect(0, BLOCK_SIZE, BLOCK_SIZE,
        BLOCK_SIZE));
  mob2.setTextureRect(sf::IntRect(2 * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE,
        BLOCK_SIZE));
  monster.print_mob(map, mob, window);
  sf::Time time = clock.getElapsedTime();
  if (time.asSeconds() > 2.0f)
    monster1.print_mob(map, mob1, window);
  if (time.asSeconds() > 4.0f)
    monster2.print_mob(map, mob2, window);
}

void init(std::ifstream& open_file, sf::Texture& tile_tex,
    sf::Sprite& tile, sf::Sprite& mob, sf::Sprite& mob1, sf::Sprite& mob2,
    matrix& map, std::vector<Tile>& tmp_map,
    int& width, int& height, Direction& d)
{
  if (open_file.is_open())
  {
    std::string tileset;
    open_file >> tileset;
    tile_tex.loadFromFile(tileset);
    tile.setTexture(tile_tex);
    mob.setTexture(tile_tex);
    mob1.setTexture(tile_tex);
    mob2.setTexture(tile_tex);
    while (!open_file.eof())
    {
      std::string line;
      open_file >> line;
      if (height == 0)
        width = line.length();
      for (unsigned long x = 0; x < line.length(); ++x)
        tmp_map = Map::parse_line(line, tmp_map, x, height, d);
      if (open_file.peek() == '\n')
      {
        height++;
        map.push_back(tmp_map);
        tmp_map.clear();
      }
    }
  }
}
int screen_1::Run(sf::RenderWindow &window)
{
  Direction d = Direction::RIGHT;
  Mob monster(100, 100, 0, BLOCK_SIZE, 100, 100);
  Mob monster1(100, 100, 0, BLOCK_SIZE, 100, 100);
  Mob monster2(100, 100, 0, BLOCK_SIZE, 100, 100);
  Game game;
  Map main_map;
  Tower tb(5, 5, 1, 10, 10, 10);
  game.add_tower(tb);
  int width = 0;
  int height = 0;
  std::ifstream open_file("map.txt");
  sf::Texture tile_tex;
  sf::Sprite tile;
  sf::Sprite mob;
  sf::Sprite mob1;
  sf::Sprite mob2;
  matrix map;
  std::vector<Tile> tmp_map;
  sf::Clock clock;
  init(open_file, tile_tex, tile,  mob, mob1, mob2,
      map, tmp_map, width, height, d);
  check_map(map);
  main_map.set_map(map);
  int x = width * BLOCK_SIZE + 200;
  int y = height * BLOCK_SIZE;
  main_map.set_size(sf::Vector2i(x, y));
  sf::Event event;
  bool running = true;
  while (running)
  {
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
        return -1;
      if (event.type == sf::Event::MouseButtonPressed)
        if (event.mouseButton.button == sf::Mouse::Left)
        {
          Tile tile = game.clicked_tile(map, window);
          game.place_tower(tile, map);
        }
    }
    window.clear();
    game.print_background(window);
    main_map.set_map(map, tile, window);
    game.print_hud(main_map.get_size(), event, window);
    print_all_mob(monster, monster1, monster2, window,
        clock, mob, mob1, mob2, map);
    window.display();
  }
  return -1;
}
