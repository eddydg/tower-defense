#ifndef TOWER_HH_
# define TOWER_HH_

# include <list>
# include "../Mob/mob.hh"
# include "../Map/map.hh"

class Tower
{
public:
    Tower(int damage, float range, float rate_of_fire,
          float x, float y, int price);
    int get_damage() const;
    int get_price() const;
    float get_range() const;
    float get_rate_of_fire() const;
    float get_x() const;
    float get_y() const;
    void attack(std::list<Mob> m);
    bool in_range(float x, float y, float x2, float y2);

protected:
    int damage_;
    float range_;
    float rate_of_fire_;
    float x_;
    float y_;
    int price_;
};

#endif /* !TOWER_HH_ */
