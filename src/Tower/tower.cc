#include "tower.hh"
#include "../Mob/mob.hh"
#include "../Map/map.hh"

Tower::Tower(int damage, float range, float rate_of_fire,
             float x, float y, int price)
      : damage_(damage)
      , range_(range)
      , rate_of_fire_(rate_of_fire)
      , x_(x)
      , y_(y)
      , price_(price)
{};

int Tower::get_price() const
{
    return price_;
}

int Tower::get_damage() const
{
    return damage_;
}

float Tower::get_range() const
{
    return range_;
}

float Tower::get_rate_of_fire() const
{
    return rate_of_fire_;
}

float Tower::get_x() const
{
    return x_;
}

float Tower::get_y() const
{
    return y_;
}

bool Tower::in_range(float x, float y, float x2, float y2)
{
    float a = x2 - x;
    float b = y2 - y;
    float calcul = std::pow(a, 2) + std::pow(b, 2) - std::pow(range_, 2);
    return calcul == 0;
}
void Tower::attack(std::list<Mob> m)
{
    for (auto &i : m)
    {
        if (in_range(x_, y_, i.get_x(), i.get_y()))
            i.attacked(damage_);
    }
}
